package br.me.calculo;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;

public class INSS {
	
	public static final String PORTARIA = "Portaria SEPRT 3.659/2020";

	public static final String VIGENCIA = "Vig�ncia de 01.03.2020 a 31.12.2020";

	private static final  Double[] LIMITE = { 1045.00D, 2089.60D, 3134.40D, 6101.06D };

	private static final  Double[] ALIQUOTA = { 0.075D, 0.09D, 0.12D, 0.14D };

	public static Double aliq(int i) {
		return ALIQUOTA[i - 1];
	}

	public static Double lim(int i) {
		return LIMITE[i - 1];
	}

	public static Double calcular(Double v) {

		// Ler ALIQ(i), LIM(i)

		// FX(i) = 0.0
		Map<Integer, Double> faixa = new HashMap<Integer, Double>();

		// Se V < LIM(1) ent�o FX(1) = V sen�o FX(1) = LIM(1)
		if (v < lim(1)) faixa.put(1, v); else faixa.put(1, lim(1));

		// Para i de 2 a 5 fa�a
		for (int i = 2; i < 5; i++) {

			// se V > LIM(i-1) AND V < LIM(i)
			if (v > lim(i - 1) && v <= lim(i)) {

				// ent�o FX(i) = V - LIM(i-1)
				faixa.put(i, v - lim(i - 1));
			}
		}

		// Para i de 2 a 5 fa�a
		for (int i = 2; i < 5; i++) {

			// se V > LIM(i)
			if (v > lim(i)) {

				// ent�o FX(i) = LIM(i) - LIM(i-1)
				faixa.put(i, lim(i) - lim(i - 1));
				
			}
			
		}

		// IN = 0.0
		Double in = 0D;

		// Para i de 1 a 5 fa�a
		for (int i = 1; i < 5; i++) {

			if (i <= faixa.values().size()) {
				
				// IN = IN + FX(i) * ALIQ(i)
				in += faixa.get(i) * aliq(i);
			}
		}

		return round(in);
	}

	public static Double truncate(Double valor) {
		BigDecimal bigvalor = BigDecimal.valueOf(valor);
		return bigvalor.setScale(2, RoundingMode.DOWN).doubleValue();
	}
	
	public static double round(double valor) {		
		return BigDecimal.valueOf(valor).setScale(2, RoundingMode.UP).doubleValue();
	}
	
}
